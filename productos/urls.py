from django.conf.urls import include, url
from django.contrib import admin

from productos import views

urlpatterns = [
    url('lista/', views.lista, name='lista')
]
